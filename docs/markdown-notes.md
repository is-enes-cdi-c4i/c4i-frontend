# NOTES for documentation using markdown

The C4I documentation can be edited directly in gitlab.
There is a distinctive repository on gitlab for the documentation content:
https://gitlab.com/is-enes-cdi-c4i/c4i-frontend-content

The current POC (proof of concept) implementation in the frontend expects as a starting point to have "test.md" file in the "c4i-frontend-content" directory.

When updating the content of the documentation there is typically NO need to update the frontend code.
https://gitlab.com/is-enes-cdi-c4i/c4i-frontend

When in the future the documentation starting point will be changed then there must be a change in the source of the c4i-frontend code.
See Content.jsx file in the feature branch https://gitlab.com/is-enes-cdi-c4i/c4i-frontend/blob/mk-feature-markdown-doc-images-relative-urls/src/Content.jsx
or the master branch: https://gitlab.com/is-enes-cdi-c4i/c4i-frontend/blob/master/src/Content.jsx

```
const url = config.frontendContentURL + '/test.md';
// ** EXAMPLE of starting point when deployed on AWS S3 bucket **
// const url = 'http://c4i-frontend-content-test-temp.s3-website-eu-west-1.amazonaws.com/test.md';
```

## Documentation guidelines

* Small typo's and minor corrections may be directly fixed in the production branch (for now the master branch).
* A good practise is to do significant changes in documentation in a separate branch (a feature branch).
  We are planning to have a testing AWS S3 bucket for new documentation which can be coupled with a (development /) test version of the web-portal front-end which can run locally by the developer. In that case the developer / documentation writer has to change "config.frontendContentURL" appropriatelly to connect the web-portal front-end with the testing AWS S3 bucket. See details below on configuration.

## Images in MARKDOWN

Upload images into gitlab repository: 

https://gitlab.com/is-enes-cdi-c4i/c4i-frontend-content/tree/master/images

In markdown documents use ONLY relative path to images.
After committing the changes in *.md and new images into gitlab repository and pushing the changes into the master a CI/CD pipeline will push the new documentation content into respective AWS S3 bucket. The full name of the S3 bucket is configured in GITLAB and may change in the future for a different URL.

The front-end  React/Javascript code will turn the relative image path into full URL to images in the AWS bucket.
Current replacements: 
```
'images/' => frontendContentURL + '/' + 'images/'
Where frontendContentURL can be something like: 'http://c4i-frontend-content-master-temp.s3-website-eu-west-1.amazonaws.com'
```

A default S3 URL to the content store can be found in: *./src/static/configDefault.js*

```
configDefault.frontendContentURL = 'http://c4i-frontend-content-master-temp.s3-website-eu-west-1.amazonaws.com';
```
The file *./src/static/config.js* can be used to specify a new, user-specified S3 URL which can be provided by CI/CD in gitlab via a variable *C4I_FRONTEND_CONTENT_S3_DNS*.

```
configDefault.frontendContentURL = 'http://C4I_FRONTEND_CONTENT_S3_DNS';
// C4I_FRONTEND_CONTENT_S3_DNS .. will be replaced by CI/CD in gitlab
```

## Automatic uploading of images and markdown documentation into AWS S3 bucket

This is will be implemented later using CI/CD pipeline in gitlab in the repository: https://gitlab.com/is-enes-cdi-c4i/c4i-frontend-content
At this stage the documentation content can be ONLY uploaded manually.

## Manual uploading of images and markdown documentation into AWS S3 bucket

First you have to install the package aws cli.  
When you never have worked with aws: mkdir -p ~/.aws  
make a file .aws/config: numbers = knmi-iam identity
 ```
 [profile c4i]
 role_arn = arn:aws:iam::[numbers]:role/knmi-operator
 source_profile = default
 region = eu-west-1

```
Make a file .aws/credentials. The credentials are created by AWS.

```
[c4i]
aws_access_key_id = [key_id]
aws_secret_access_key = [access_key]

```

Add the documentation to the S3 bucket:
```
[c4i-frontend-content]$ # ...DIR.../is-enes-c4i/c4i-frontend-content

export c4ifrontend_content_dir=`pwd`    
export c4ifrontend_content_s3bucket_name=c4i-frontend-content-master-temp

aws s3 sync ${c4ifrontend_content_dir} s3://${c4ifrontend_content_s3bucket_name} --exclude ".git/*" --exclude ".restricted-info/*" --grants read=uri=http://acs.amazonaws.com/groups/global/AllUsers --cache-control "no-cache, no-s" --profile c4i
```
