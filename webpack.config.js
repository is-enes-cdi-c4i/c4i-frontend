const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
require('babel-register');

module.exports = {
  entry: './src/main.js',
  cache: false,
  mode:'development',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'c4i-frontend.js'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        use: [
          {
            loader: require.resolve('awesome-typescript-loader'),
            options: {
              configFileName: './tsconfig.json'
            }
          },
          {
            loader: require.resolve('react-docgen-typescript-loader')
          }
        ]
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      }, {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader',
            options: { minimize: true }
          }
        ]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.jpe?g$|\.ico$|\.gif$|\.png$|\.svg$|\.woff$|\.woff2$|\.eot$|\.ttf$|\.wav$|\.mp3$/,
        loader: 'file-loader?name=[name].[ext]' // <-- retain original file name
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', 'scss']
  },
  externals:{
    jquery: 'jQuery',
    moment: 'moment'
  },
  plugins: [
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html'
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css'
    }),
    new CopyWebpackPlugin([ { from: 'src/static' } ]),
    new webpack.HotModuleReplacementPlugin()
  ],
  devServer: {
    overlay: false,
    hot: true
  },
  watchOptions: {
    // ignored: /node_modules/
  }
};
module.loaders = [
  { test: /\.js$/, exclude: /node_modules/, use: 'babel-loader' }
];
