const configDefault = {};

configDefault.frontendContentURL = 'http://c4i-frontend-content-dev-temp.s3-website-eu-west-1.amazonaws.com';
configDefault.c4iSearchPortalBackend = 'https://34.254.195.81/c4i-search-portal-backend/';
configDefault.catalogBackendUrl = 'https://34.254.195.81/adaguc-services/';
configDefault.HostnameUrl = configDefault.catalogBackendUrl + 'wms?source=';
configDefault.WMSserviceUrl = '&service=wms&request=getcapabilities';
configDefault.WCSserviceUrl = '&service=wcs&request=getcapabilities';
configDefault.MAPserviceUrl = '&service=wms&request=getmap&width=1000&format=image/png&LAYERS=tasmax&title=ECEarth%20Tasmax%20test&CRS=EPSG:4326&';
configDefault.MetadataServiceUrl = '&service=metadata&request=getmetadata';
//configDefault.frontendContentURL = 'http://C4I_FRONTEND_CONTENT_S3_DNS';
// C4I_FRONTEND_CONTENT_S3_DNS .. will be replaced by CI/CD in gitlab
module.exports = configDefault;
