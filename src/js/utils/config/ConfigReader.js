let configDefault = require('../../../static/configDefault');

export const getConfig = function () {
  let c;
  if (typeof (config) === 'undefined') {
    // eslint-disable-next-line no-undef
    c = Object.assign({}, configDefault);
  } else {
    // eslint-disable-next-line no-undef
    c = Object.assign({}, configDefault, config || {});
  }
  return c;
};
