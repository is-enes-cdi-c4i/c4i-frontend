import PropTypes from 'prop-types';
import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';
import { getConfig } from './config/ConfigReader';
import NotFoundPage from '../components/routes/not-found.route';

let config = getConfig();
const replaceFunctions = [
  {
    search: 'images/',
    replace: `${config.frontendContentURL}/images/`
  },
  {
    search: 'pages/',
    replace: `#/pages/`
  }
];
export default class MarkdownFromFile extends Component {
  constructor () {
    super();

    this.updateContent = this.updateContent.bind(this);

    this.state = {
      text: 'Loading content...'
    };
  }

  updateContent () {
    let { url: fileUrl } = this.props;

    // NOT a Redux-reducer
    const replacerReducer = (result, { search, replace }) => result.replace(new RegExp(search, 'g'), replace);
    const handleNotFound = () => {
      this.setState({
        text: <NotFoundPage url={window.location.hash} />
      });
    };
    //  fetch markdown-file, execute replacer-functions, set result to text-state
    fetch(fileUrl)
      .then(response => response.ok ? response.text() : Promise.reject(new Error('-')))
      .then(result => replaceFunctions.reduce(replacerReducer, result))
      .then(result => this.setState({ text: result }))
      .catch(handleNotFound);
  }

  componentDidMount () {
    // Listen to hashchange-event to update content accordingly
    window.addEventListener('hashchange', this.updateContent);

    this.updateContent();
  }

  componentWillUnmount () {
    // Stop listening to hashchange-event when demounting
    window.removeEventListener('hashchange', this.updateContent);
  }

  render () {
    return typeof this.state.text === 'string' ? <ReactMarkdown source={this.state.text}
      escapeHtml={false} /> : this.state.text;
  }
}
MarkdownFromFile.propTypes = {
  url: PropTypes.string
};
