import React from 'react';
import { connect } from 'react-redux';
import { hot } from 'react-hot-loader';
import 'bootstrap/dist/css/bootstrap.min.css';
import style from '../../styles/main.css'; // eslint-disable-line no-unused-vars
import navbar from '../../styles/navbar.css'; // eslint-disable-line no-unused-vars
import Header from './Header';
import Content from './Main';
require('../../img/favicon.ico');

const App = () => {
  return (<div className='wrapper'>
    <div className='header'><Header /></div>
    {/* <div className='sidebar'>Sidebar</div> */}
    <div className='content'><Content /></div>
    <div className='footer'>
      <img className='c4i-footer-euflag' src='./images/euflag.png' />
      <span className='c4i-footer-text'>
        This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 824084
      </span>
    </div>
  </div>);
};
export default hot(module)(connect(null)(App));
