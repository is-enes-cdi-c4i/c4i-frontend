import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Nav, Navbar, NavbarToggler, Collapse, NavItem, NavLink } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ErrorWrapper, ERRORHANDLER_NAME, errorHandler } from '@c4i/errorhandler';

class Header extends Component {
  constructor (props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle () {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  render () {
    const { location } = this.props;

    const navItemLabels = [
      {
        path: '/pages/welcome.md',
        label: 'Home'
      },
      {
        path: '/search',
        label: 'Data Discovery'
      },
      // {
      //   path: '/calculate',
      //   label: 'Downscaling'
      // },
      // {
      //   path: '/documentation',
      //   label: 'Documentation'
      // },
      // // {
      // //   path: '/about',
      // //   label: 'About us'
      // // },
      // {
      //   path: '/help',
      //   label: 'Help'
      // },
      // {
      //   path: '/account',
      //   label: 'Account'
      // },
      {
        path: '/feedback',
        label: 'Feedback'
      }
    ];
    navItemLabels.forEach((item) => {
      item.active = item.path === location.pathname;
    });
    // Utility component-functions: Logo, NavItem
    // noinspection HtmlUnknownTarget
    const HeaderLogo = () => <div className='c4i-header'><div className='c4i-header-logo'>
      <a href='#'><img src='./images/IS-ENES3_logo_small.png' alt='is-enes logo' /></a>
    </div><div className='c4i-header-text'>Exploring climate model data</div></div>;
    const HeaderNavItem = ({ route, title, activePage }) => <NavItem navbar><NavLink active={activePage} href={`#${route}`}>{title}</NavLink></NavItem>;
    const navItems = navItemLabels.map((item) => <HeaderNavItem route={item.path}
      title={item.label}
      key={item.path} activePage={item.active} />);

    return (<header>
      <div className='error-view' style={{
        display: (this.props && this.props.errors && this.props.errors.length > 0) ? 'block' : 'none'
      }}>
        <ErrorWrapper />
      </div>
      <HeaderLogo />
      <div className='navbar-wrapper'>
        <Navbar light expand='md' className='navbar-static-top navbar'>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav>
              {navItems}
            </Nav>
          </Collapse>
          <form className='form-inline ml-auto'>
            <input className='form-control test' type='text' placeholder='Search' aria-label='Search' />
            <div className='input-group-append'>
              <span className='input-group-text amber lighten-3' id='basic-text1'>
                <i className='fas fa-search text-grey' aria-hidden='true' />
              </span>
            </div>
          </form>
        </Navbar>
      </div>
    </header>);
  }
}

const mapStateToPropsHeader = state => {
  const errorHandlerState = state[ERRORHANDLER_NAME] ? state[ERRORHANDLER_NAME] : errorHandler();
  return {
    errors: errorHandlerState.errors
  };
};

/* PropTypes */
Header.propTypes = {
  errors: PropTypes.array.isRequired,
  location: PropTypes.any
};

export default withRouter(connect(mapStateToPropsHeader)(Header));
