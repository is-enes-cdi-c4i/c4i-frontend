import React from 'react';
import PropTypes from 'prop-types';

const FeedbackPage = ({ feedbackLink }) =>
  (<div className='d-flex justify-content-center'>
    <iframe width='1000' height='2050' scrolling='no' src={feedbackLink} />
  </div>);

FeedbackPage.propTypes = {
  feedbackLink: PropTypes.string
};

FeedbackPage.defaultProps = {
  feedbackLink: 'https://docs.google.com/forms/d/e/1FAIpQLSftLFmFgw9Z2WlCyaj89GghX5VycrfRv6B0MWRkjqjqpIzzIA/viewform'
};

export default FeedbackPage;
