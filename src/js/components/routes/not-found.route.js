import PropTypes from 'prop-types';
import React, { Component } from 'react';

class NotFoundPage extends Component {
  render () {
    let { url } = this.props;

    return (
      <div className='markdown'>
        <h5>This is a Beta version of the Portal. <br /><br /> Work in progress...</h5>
        {url ? <p>Page {url.substr(1)} was not found</p> : null}
      </div>
    );
  }
}

NotFoundPage.propTypes = {
  url: PropTypes.string
};

export default NotFoundPage;
