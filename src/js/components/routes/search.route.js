import { SearchPortalApp } from '@c4i/esgf-search';
import { DatasetPreview } from '@c4i/dataset-preview';
// eslint-disable-next-line no-unused-vars
// noinspection ES6UnusedImports
// eslint-disable-next-line no-unused-vars
import * as Styles from '@c4i/esgf-search/dist/main.css';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { getConfig } from '../../utils/config/ConfigReader';
import { errorHandlerAdd } from '@c4i/errorhandler';
import { connect } from 'react-redux';
let config = getConfig();

class SearchPage extends Component {
  constructor () {
    super();
    this.state = { dataset : null };
  }

  render () {
    const { dispatch } = this.props;
    let urlConfig = {
      facetUrl:   config.c4iSearchPortalBackend,
      searchUrl:  config.c4iSearchPortalBackend,
      catalogUrl: config.catalogBackendUrl,
      hostnameUrl:    config.hostnameUrl,
      WMSserviceUrl:  config.WMSserviceUrl,
      WCSserviceUrl:  config.WCSserviceUrl,
      MAPserviceUrl:  config.MAPserviceUrl,
      MetadataServiceUrl:  config.MetadataServiceUrl
    };

    return (
      <div>
        <div style={{ display:this.state.dataset === null ? 'block' : 'none' }}>
          <SearchPortalApp
            backendUrls={urlConfig} env='prod' viewDatasetHandler={(dataset) => { this.setState({ dataset : dataset }); }} />
        </div>
        <div style={{ display:this.state.dataset === null ? 'none' : 'block' }}>
          {this.state.dataset != null &&
            <DatasetPreview
              dataset={this.state.dataset}
              backendUrls={urlConfig}
              errorHandler={(message, error) => {
                console.log('dispatching error', message, error);
                dispatch(errorHandlerAdd(message));
              }}
              closeHandler={() => {
                this.setState({ dataset : null });
              }}
            />
          }
        </div>
      </div>
    );
  }
}

/* PropTypes */
SearchPage.propTypes = {
  dispatch: PropTypes.func
};

export default connect(() => { return {}; })(SearchPage);
