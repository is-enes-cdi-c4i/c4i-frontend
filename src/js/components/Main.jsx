import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { getConfig } from '../utils/config/ConfigReader';
import NotFoundPage from './routes/not-found.route';
import SearchPage from './routes/search.route';
import ReactMarkdown from '../utils/MarkdownRenderer';
import FeedbackPage from './FeedbackPage';

let config = getConfig();
console.log(config);

/**
 * @summary creates md-pages url from (relative) route
 * @param {string} route
 */
const createUrl = (route) => {
  // hash and pages are ignored
  let [, , ...path] = route.split('/');
  let url = `${config.frontendContentURL}/pages/${path.join('/')}`;
  return url.substr(-3) === '.md' ? url : url + '.md';
};

class Main extends Component {
  render () {
    let NotFound = () => <NotFoundPage />;

    return (<div className='mainComponent'>
      <Switch>
        <Redirect exact from='/' to='/pages/welcome.md' />
        <Redirect from='/home' to='/' />
        <Route path={'/pages'} component={() => <div className='markdown'>
          <ReactMarkdown url={createUrl(window.location.hash)} />
        </div>} />
        <Route path='/search' component={SearchPage} />
        <Route path='/notfound' component={NotFound} />
        <Route path='/feedback' component={FeedbackPage} />
        <Route component={NotFound} />
      </Switch>
    </div>
    );
  }
}

const mapStateToPropsMain = state => {
  /* Empty state */
  return {};
};

export default connect(mapStateToPropsMain)(Main);
