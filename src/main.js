import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { HashRouter } from 'react-router-dom';

import App from './js/components/App.jsx';
import store from './js/utils/redux/initializeReduxStore';

ReactDOM.render(<Provider store={store}>
  <HashRouter>
    <App />
  </HashRouter>
</Provider>, document.getElementById('app'));
