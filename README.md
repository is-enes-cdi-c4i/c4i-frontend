# c4i-frontend

## To start developing:

- git clone this repository
- cd into this directory
- Get npm, it is often useful to use nvm to manage npm environments: https://github.com/nvm-sh/nvm
- npm install
- npm run start

The code has been editted with Visual Studio Code. If you add this folder to VSC, linting, hot reloading is arranged.